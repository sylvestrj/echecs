package Jeu;

import Piece.*;
import sun.rmi.runtime.NewThreadAction;


import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/*TODO dans les mouvements possibles ajouté la case finale du déplacement uniquement si c'est d'une
couleur différente ET faire attention pour le cas du roi (car on peut pas le prendre :) */
public class Board {
    private Case[][] allCase;
    static private boolean white;
    public static final int TAILLE = 8;
    private Coordonnees coordonneesRoiBlanc;
    private Coordonnees coordonneesRoiNoir;

    private LinkedList<Coup> allCoup;
    private boolean wKingMoved;
    private boolean wTourMovedLeft;
    private boolean wTourMovedRight;
    private boolean bKingMoved;
    private boolean bTourMovedLeft;
    private boolean bTourMovedRight;

    public Board() {
        setWhite(true);

        allCoup = new LinkedList<>();
        allCase = new Case[8][8];
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                if (i == 1 || i == 6) {
                    if (i == 1) {
                        allCase[i][j] = new Case(new Pion(false));
                    } else {
                        allCase[i][j] = new Case(new Pion(true));
                    }
                } else if ((j == 0 || j == 7) && (i == 0 || i == 7)) {

                    if (i == 0) {
                        allCase[i][j] = new Case(new Tour(false));
                    } else {
                        allCase[i][j] = new Case(new Tour(true));
                    }

                } else if ((j == 1 || j == 6) && (i == 0 || i == 7)) {

                    if (i == 0) {
                        allCase[i][j] = new Case(new Cavalier(false));
                    } else {
                        allCase[i][j] = new Case(new Cavalier(true));
                    }
                } else if ((j == 2 || j == 5) && (i == 0 || i == 7)) {

                    if (i == 0) {
                        allCase[i][j] = new Case(new Fou(false));
                    } else {
                        allCase[i][j] = new Case(new Fou(true));
                    }
                } else if (j == 3 && (i == 0 || i == 7)) {

                    if (i == 0) {
                        allCase[i][j] = new Case(new Reine(false));
                    } else {
                        allCase[i][j] = new Case(new Reine(true));
                    }
                } else if (j == 4 && (i == 0 || i == 7)) {
                    if (i == 0) {
                        allCase[i][j] = new Case(new Roi(false));
                        coordonneesRoiNoir = new Coordonnees(j, i);
                    } else {
                        allCase[i][j] = new Case(new Roi(true));
                        coordonneesRoiBlanc = new Coordonnees(j, i);

                    }
                } else {
                    allCase[i][j] = new Case();
                }
            }

        }

    }

    public Board(boolean debug) {

        setWhite(true);

        allCoup = new LinkedList<>();
        allCase = new Case[TAILLE][TAILLE];
        for (int i = 0; i < TAILLE; i++) {
            for (int j = 0; j < TAILLE; j++) {
                allCase[i][j] = new Case();
            }

        }

    }

    public boolean isWhite() {
        return white;
    }

    public void setWhite(boolean white) {
        Board.white = white;
    }

    public Coordonnees getCoordonneesRoiBlanc() {
        return coordonneesRoiBlanc;
    }

    public void setCoordonneesRoiBlanc(Coordonnees coordonneesRoiBlanc) {
        this.coordonneesRoiBlanc = coordonneesRoiBlanc;
    }

    public Coordonnees getCoordonneesRoiNoir() {
        return coordonneesRoiNoir;
    }

    public void setCoordonneesRoiNoir(Coordonnees coordonneesRoiNoir) {
        this.coordonneesRoiNoir = coordonneesRoiNoir;
    }

    /**
     * Déplace un pion sur le plateau
     * Entrées :
     * - move : Chaine de caratère au format lettre-chiffre-lettre-chiffre (exemple d3d4)
     */
    public void moveString(String move)//Type d2d4
    {
        Coup coup = new Coup(move);
        moveCoup(coup);

    }

    public void moveCoup(Coup coup) {

        int startingColumn = coup.getCoordonneesDepart().getColonne();
        int startingLine = coup.getCoordonneesDepart().getLigne();

        int endColumn = coup.getCoordonneesCible().getColonne();
        int endLine = coup.getCoordonneesCible().getLigne();
        coup.setPiecePrise(allCase[endLine][endColumn].getPiece());
        coup.updateScoreFavorizeMiddle();

        //System.out.println(move+" traduction :"+startingLine+":"+startingColumn+"->"+endLine+":"+endColumn);
        Piece pieceCible = allCase[startingLine][startingColumn].getPiece();

        //On met à jour les coordonnées du roi
        if (pieceCible instanceof Roi) {
            boolean coteRoi = pieceCible.getCote();
            if (coteRoi) coordonneesRoiBlanc = coup.getCoordonneesCible();
            else coordonneesRoiNoir = coup.getCoordonneesCible();
        }


        if (pieceCible instanceof Roi) //Si la piece est un roi
        {
            if (pieceCible.getCote() && !hasWKingMoved())//Si le Roi est blanc et n'a pas déjç bougé
            {
                setwKingMoved(true);//On dit qu'il a bougé
            }
            if ((!pieceCible.getCote()) && !hasBKingMoved())//Si le Roi est blanc et n'a pas déjç bougé
            {
                setbKingMoved(true);  //
            }

            if (Math.abs(startingColumn - endColumn) == 2) // Si on se déplace de deux cases vers la gauche ou la droite
            {

                if (allCase[endLine][endColumn + 1].getPiece() != null) {//Si c'est ver la droite, on regarde au dela
                    allCase[endLine][endColumn - 1].setPiece(allCase[endLine][endColumn + 1].getPiece());//
                    allCase[endLine][endColumn + 1].setPiece(null);

                }
            }
            if (Math.abs(startingColumn - endColumn) == 3) // Si on se déplace de trois cases
            {
                if (allCase[endLine][endColumn - 1].getPiece() != null) {
                    allCase[endLine][endColumn + 1].setPiece(allCase[endLine][endColumn - 1].getPiece());
                    allCase[endLine][endColumn - 1].setPiece(null);

                }
            }

        }
        if (pieceCible instanceof Tour)//Si on bouge une tour
        {
            if (pieceCible.getCote()) //On regarde sa couleur
            {
                if ((!hasWTourMovedLeft()) && startingColumn == 0)// Si la tour de gauche n'a pas bougé et que la tour que l'on bouge se trouve sur la huitième colonne
                {
                    setwTourMovedLeft(true);
                }
                if ((!hasWTourMovedRight()) && startingColumn == 7)// Si la tour de gauche n'a pas bougé et que la tour que l'on bouge se trouve sur la huitième colonne
                {
                    setwTourMovedRight(true);
                }

            }
            if (!pieceCible.getCote()) //On regarde sa couleur
            {
                if ((!hasBTourMovedLeft()) && startingColumn == 0)// Si la tour de gauche n'a pas bougé et que la tour que l'on bouge se trouve sur la huitième colonne
                {
                    setbTourMovedLeft(true);
                }
                if ((!hasBTourMovedRight()) && startingColumn == 7)// Si la tour de gauche n'a pas bougé et que la tour que l'on bouge se trouve sur la huitième colonne
                {
                    setbTourMovedRight(true);
                }

            }

        }
        allCase[endLine][endColumn].setPiece(pieceCible);
        allCase[startingLine][startingColumn].setPiece(null);


        //Si la pièce est un pion et arrive au bout du board, alors on la transforme en reine
        if (pieceCible.TypePiece() == TypePiece.Pion && (endLine == 8 || endLine == 0)) {
            allCase[endLine][endColumn].setPiece(new Reine(pieceCible.getCote()));
            coup.setTypeCoup(Coup.TypeCoup.Promotion);
        }

        allCoup.add(coup);

        setWhite(!isWhite());

    }

    public void undoLastCoup() {
        if (allCoup.size() > 0) {
            Coup lastCoup = allCoup.removeLast();

            int startingColumn = lastCoup.getCoordonneesDepart().getColonne();
            int startingLine = lastCoup.getCoordonneesDepart().getLigne();

            int endColumn = lastCoup.getCoordonneesCible().getColonne();
            int endLine = lastCoup.getCoordonneesCible().getLigne();

            Piece pieceCible = allCase[endLine][endColumn].getPiece();

            //On met à jour les coordonnées du roi
            if (pieceCible instanceof Roi) {
                boolean coteRoi = pieceCible.getCote();
                if (coteRoi) coordonneesRoiBlanc = lastCoup.getCoordonneesDepart();
                else coordonneesRoiNoir = lastCoup.getCoordonneesDepart();
            }

            //TODO le cas du roc

            if (pieceCible instanceof Roi) {
                if (Math.abs(startingColumn - endColumn) == 2)//
                {
                    //Repositionnement de la tour  à droite
                    allCase[endLine][endColumn + 1].setPiece(allCase[endLine][endColumn - 1].getPiece());
                    allCase[endLine][endColumn - 1].setPiece(null);

                }
                if (Math.abs(startingColumn - endColumn) == 3) {
                    //Repositionnement de la tour  à gauche
                    allCase[endLine][endColumn - 1].setPiece(allCase[endLine][endColumn + 1].getPiece());
                    allCase[endLine][endColumn + 1].setPiece(null);

                }

            }
            //Si il y a eu promotion
            if (lastCoup.getTypeCoup() == Coup.TypeCoup.Promotion) {
                pieceCible = new Pion(pieceCible.getCote());
            }

            //repositionnement de la piece
            allCase[startingLine][startingColumn].setPiece(pieceCible);

            //Résurection de la piece prise s'il y en a une
            allCase[endLine][endColumn].setPiece(lastCoup.getPiecePrise());

            setWhite(!isWhite());
        }
    }


    /**
     * Permet de dire si la couleur donné en paramètre est échec ou non
     *
     * @param couleurRoi
     * @return
     */
    public boolean echec(Coordonnees coordonneesDuRoi, boolean couleurRoi) {

        int colonne = coordonneesDuRoi.getColonne();
        int ligne = coordonneesDuRoi.getLigne();

        //On regarde si le roi se fait attaquer par un fou ou Dame
        List<Coup> coordonneesPossibleFou = Fou.mouvementsPossibleFou(this, coordonneesDuRoi);
        for (Coup coupFou : coordonneesPossibleFou) {
            Coordonnees coordonneesFou = coupFou.getCoordonneesCible();
            Piece piece = allCase[coordonneesFou.getLigne()][coordonneesFou.getColonne()].getPiece();
            if ((piece instanceof Fou || piece instanceof Reine) && piece.getCote() != couleurRoi) {
//                System.out.println("Echec par le fou ou dame");
                return true;
            }
        }


        //On regarde si le roi se fait attaquer par une tour
        List<Coup> coordonneesPossibleTour = Tour.mouvementsPossibleTour(this, coordonneesDuRoi);
        for (Coup coupTour : coordonneesPossibleTour) {
            Coordonnees coordonneesTour = coupTour.getCoordonneesCible();
            Piece piece = allCase[coordonneesTour.getLigne()][coordonneesTour.getColonne()].getPiece();
            if ((piece instanceof Tour || piece instanceof Reine) && piece.getCote() != couleurRoi) {
//                System.out.println("Echec par la tour ou dame");
                return true;
            }
        }


        //On regarde si on se fait attaquer par un cavalier
        List<Coup> coordonneesPossibleCavalier = Cavalier.mouvementsPossibleCavalier(this, coordonneesDuRoi);
        for (Coup coupCavalier : coordonneesPossibleCavalier) {

            Coordonnees coordonneesCavalier = coupCavalier.getCoordonneesCible();

            Piece piece = allCase[coordonneesCavalier.getLigne()][coordonneesCavalier.getColonne()].getPiece();
            if (piece instanceof Cavalier && piece.getCote() != couleurRoi) {
//                System.out.println("Echec par le cavalier");
                return true;
            }
        }


        if (colonne < 7)//vérification à droite
        {
            if (ligne > 0 && couleurRoi) {
                Piece piece = allCase[ligne - 1][colonne + 1].getPiece();
                if (piece instanceof Pion && piece.getCote() != couleurRoi) {
//                    System.out.println("Echec par le pion");
                    return true;
                }
            }
            if (ligne < 7 && !couleurRoi)//J'ai mis cettte disposition pour tenter de minimiser le nombre de if
            {
                Piece piece = allCase[ligne + 1][colonne + 1].getPiece();
                if (piece instanceof Pion && piece.getCote() != couleurRoi) {
//                    System.out.println("Echec par le pion");
                    return true;
                }
            }
        }

        if (colonne > 0) { //Vérirfication à gauche
            if (ligne > 0 && couleurRoi) {
                Piece piece = allCase[ligne - 1][colonne - 1].getPiece();
                if (piece instanceof Pion && piece.getCote() != couleurRoi) {
//                    System.out.println("Echec par le pion");
                    return true;
                }
            }
            if (ligne < 7 && !couleurRoi) {
                Piece piece = allCase[ligne + 1][colonne - 1].getPiece();
                if (piece instanceof Pion && piece.getCote() != couleurRoi) {
                    //System.out.println("Echec par le pion");
                    return true;
                }
            }
        }
/*
        //On regarde si on se fait attaquer par un roi
        List<Coup> coordonneesPossibleRoi = Roi.mouvementsPossibleRoi(this, coordonneesDuRoi);
        for (Coup coupRoi : coordonneesPossibleRoi) {
            Coordonnees coordonneesRoi = coupRoi.getCoordonneesCible();
            Piece piece = allCase[coordonneesRoi.getLigne()][coordonneesRoi.getColonne()].getPiece();
            if (piece instanceof Roi && piece.getCote() != couleurRoi) {
                return true;
            }
        }
*/
        return false;
    }


    /**
     * Permet de savoir si le roi de couleur couleurRoi à la coordonnées entrées
     *
     * @param coordonneesRoi
     * @param couleurRoi
     * @return
     */
    public boolean echetMat(Coordonnees coordonneesRoi, boolean couleurRoi) {

        Coordonnees coordonneesRoiDebut = new Coordonnees(coordonneesRoi.getColonne(), coordonneesRoi.getLigne());

        if (echec(coordonneesRoi, couleurRoi)) {
            List<Coup> coordonneesPossibles = Solver.allMouvementsPossible(this, couleurRoi);
            for (Coup coup : coordonneesPossibles) {

                //Si le roi bouge
                Coordonnees coordonneesDepart = coup.getCoordonneesDepart();
                if (getAllCase()[coordonneesDepart.getLigne()][coordonneesDepart.getColonne()].getPiece() instanceof Roi) {
                    coordonneesRoi = coup.getCoordonneesCible();
                }
                moveCoup(coup);
                if (!echec(coordonneesRoi, couleurRoi)) {
                    undoLastCoup();
                    coordonneesRoi = coordonneesRoiDebut;
                    return false;
                }
                undoLastCoup();
                coordonneesRoi = coordonneesRoiDebut;

            }
            return true;
        } else {
            return false;
        }
    }



    public boolean hasWKingMoved() {
        return wKingMoved;
    }

    public void setwKingMoved(boolean wKingMoved) {
        this.wKingMoved = wKingMoved;
    }

    public boolean hasWTourMovedLeft() {
        return wTourMovedLeft;
    }

    public void setwTourMovedLeft(boolean wTourMovedLeft) {
        this.wTourMovedLeft = wTourMovedLeft;
    }

    public boolean hasWTourMovedRight() {
        return wTourMovedRight;
    }

    public void setwTourMovedRight(boolean wTourMovedRight) {
        this.wTourMovedRight = wTourMovedRight;
    }

    public boolean hasBKingMoved() {
        return bKingMoved;
    }

    public void setbKingMoved(boolean bKingMoved) {
        this.bKingMoved = bKingMoved;
    }

    public boolean hasBTourMovedLeft() {
        return bTourMovedLeft;
    }

    public void setbTourMovedLeft(boolean bTourMovedLeft) {
        this.bTourMovedLeft = bTourMovedLeft;
    }

    public boolean hasBTourMovedRight() {
        return bTourMovedRight;
    }

    public void setbTourMovedRight(boolean bTourMovedRight) {
        this.bTourMovedRight = bTourMovedRight;
    }

    /**
     * Tout moche :(
     */
    @Override
    public String toString() {
        StringBuilder str = new StringBuilder("      A          B          C          D          E          F          G          H\n");
        for (int i = 0; i < 8; i++) {
            str.append(8 - i);
            for (int j = 0; j < 8; j++) {
                if (allCase[i][j].isPiece()) {

                    str.append("|").append(allCase[i][j].getPiece().toString());
                } else
                    str.append("|          ");
            }
            str.append("|\n");
        }

        return str.toString();
    }

    public Case[][] getAllCase() {
        return allCase;
    }
}

