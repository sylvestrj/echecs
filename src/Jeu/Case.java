package Jeu;

import Piece.Piece;

public class Case {
    private boolean border; //Pour la promotion, comme aux dames quand t'a un
    // pion qui arri au côté adverse
    private Piece piece;

    public Case() {
        this.border = false;
    }


    public Case(Piece piece) {
        this.border = false;
        this.piece = piece;
    }


    public void setBorder(boolean border) {
        this.border = border;
    }

    public void setPiece(Piece piece) {
        this.piece = piece;
    }

    public boolean isBorder() {
        return border;
    }


    public boolean isPiece() {
        return this.piece != null;
    }

    public Piece getPiece() {

        return this.piece;
    }

    @Override
    public String toString() {
        return "Jeu.Case{" +
                "border=" + border +
                ", piece=" + piece +
                '}';
    }
}
