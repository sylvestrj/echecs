package Jeu;

import java.util.ArrayList;
import java.util.List;

public class Coordonnees {
    private int colonne;
    private int ligne;


    public Coordonnees(int colonne, int ligne) {
        this.colonne = colonne;
        this.ligne = ligne;
    }

    public int getLigne() {
        return ligne;
    }

    public void setLigne(int ligne) {
        this.ligne = ligne;
    }

    public int getColonne() {
        return colonne;
    }

    public void setColonne(int colonne) {
        this.colonne = colonne;
    }

    @Override
    public String toString() {
        return "Jeu.Coordonnees{" +
                "colonne=" + colonne +
                ", ligne=" + ligne +
                '}';
    }

    /**
     * Permet de renvoyer la liste de coordonnées possibles autour de la piece
     * Ne va pas envoyer des coordonnées en dehors du Board
     *
     * @return
     */
    public List<Coordonnees> coordonneesPossibles() {

        int ligne = this.getLigne();
        int colonne = this.getColonne();
        List<Coordonnees> coordonneesPossibles = new ArrayList<>();
        if (ligne > 0) {
            coordonneesPossibles.add(new Coordonnees(colonne, ligne - 1));
        }
        if (ligne < 7) {
            coordonneesPossibles.add(new Coordonnees(colonne, ligne + 1));
        }
        if (colonne > 0) {
            coordonneesPossibles.add(new Coordonnees(colonne - 1, ligne));
            if (ligne > 0) {
                coordonneesPossibles.add(new Coordonnees(colonne - 1, ligne - 1));
            }
            if (ligne < 7) {
                coordonneesPossibles.add(new Coordonnees(colonne - 1, ligne + 1));
            }
        }
        if (colonne < 7) {
            coordonneesPossibles.add(new Coordonnees(colonne + 1, ligne));
            if (ligne < 7) {
                coordonneesPossibles.add(new Coordonnees(colonne + 1, ligne + 1));
            }
            if (ligne > 0) {
                coordonneesPossibles.add(new Coordonnees(colonne + 1, ligne - 1));
            }
        }
        return coordonneesPossibles;
    }

/*    public static void main(String[] args) {
        Coordonnees coordonnees = new Coordonnees(5, 0);
        System.out.println(Coup.parseCoup(coordonnees));
    }*/
}
