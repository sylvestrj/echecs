package Jeu;

import Piece.Piece;

public class Coup {

    private Piece piece;
    private Coordonnees coordonneesDepart;
    private Coordonnees coordonneesCible;
    private Piece piecePrise;
    private int score;
    private Coup coupSuivant;

    public enum TypeCoup {Mouvement, Roc, PriseEnPassant, Promotion}

    private TypeCoup typeCoup;

    public Coup(Coordonnees coordonneesDepart, Coordonnees coordonneesCible) {
        this.coordonneesDepart = coordonneesDepart;
        this.coordonneesCible = coordonneesCible;
        piecePrise = null;
        score = 0;
        typeCoup = TypeCoup.Mouvement;
    }

    public Coup(Coordonnees coordonneesDepart, Coordonnees coordonneesCible, Piece piece) {
        this.coordonneesDepart = coordonneesDepart;
        this.coordonneesCible = coordonneesCible;
        piecePrise = piece;
        score = piecePrise.score;
        typeCoup = TypeCoup.Mouvement;
    }

    public Coup(String input) {
        int startingColumn = input.charAt(0) - 'a';//Conversion du premier caractère(lettre) en chiffre
        int startingLine = '8' - input.charAt(1);
        //Conversion du deuxième caractère(chiffre), car le plateau d'échec utilise
        // un index inversé (en haut à gauche du plateau, c'est a(colonne):8(ligne), tandis qu'en java, c'est 0(ligne): 0(colonne)
        int endColumn = input.charAt(2) - 'a';
        int endLine = '8' - input.charAt(3);

        this.coordonneesDepart = new Coordonnees(startingColumn, startingLine);
        this.coordonneesCible = new Coordonnees(endColumn, endLine);
        typeCoup = TypeCoup.Mouvement;
        piecePrise = null;
        score = 0;
    }

    public Piece getPiece() {
        return piece;
    }

    public void setPiece(Piece piece) {
        this.piece = piece;
    }

    public Piece getPiecePrise() {
        return piecePrise;
    }

    public void setPiecePrise(Piece piecePrise) {
        this.piecePrise = piecePrise;
        if (piecePrise != null) {
            this.score += piecePrise.score;
        }
    }

    public void updateScoreFavorizeMiddle() {
        this.score += favorizeMiddle(coordonneesCible.getColonne());
        this.score += favorizeMiddle(coordonneesCible.getLigne());
    }

    /**
     * Permet de favoriser les déplacement au milieu
     *
     * @param numberColumnRow
     * @return
     */
    private int favorizeMiddle(int numberColumnRow) {

        if (numberColumnRow == 0 || numberColumnRow == 7) {
            return 0;
        } else if (numberColumnRow == 1 || numberColumnRow == 6) {
            return 1;
        } else if (numberColumnRow == 2 || numberColumnRow == 5) {
            return 2;
        } else if (numberColumnRow == 3 || numberColumnRow == 4) {
            return 3;
        }
        return 0;

    }


    public Coordonnees getCoordonneesCible() {
        return coordonneesCible;
    }

    public void setCoordonneesCible(Coordonnees coordonneesCible) {
        this.coordonneesCible = coordonneesCible;
    }


    public Coordonnees getCoordonneesDepart() {
        return coordonneesDepart;
    }

    public void setCoordonneesDepart(Coordonnees coordonneesDepart) {
        this.coordonneesDepart = coordonneesDepart;
    }

    public void addScore(int score) {
        this.score += score;
    }

    public void reverseScore() {
        this.score = -this.score;
    }

    public int getScore() {
        return this.score;
    }

    public TypeCoup getTypeCoup() {
        return typeCoup;
    }

    public void setTypeCoup(TypeCoup typeCoup) {
        this.typeCoup = typeCoup;
    }

    public Coup getCoupSuivant() {
        return coupSuivant;
    }

    public void setCoupSuivant(Coup coupSuivant) {
        this.coupSuivant = coupSuivant;
    }

    /**
     * Méthode permettant de parser des coordonnées en String
     *
     * @param coup
     * @return
     */
    static public String parseCoup(Coordonnees coup) {
        return (char) ('a' + coup.getColonne()) + "" + (char) ('8' - coup.getLigne());
    }

    @Override
    public String toString() {
        return "Coup{" +
                " Depart = " + parseCoup(coordonneesDepart) +
                ", Cible = " + parseCoup(coordonneesCible) +
                (piecePrise != null ? ", Pièce prise : " + piecePrise : "") +
                (coupSuivant != null ? ", Coup suivant : " + coupSuivant : "") +
                ", Score : " + score +
                '}';
    }
}
