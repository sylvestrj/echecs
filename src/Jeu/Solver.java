package Jeu;

import Piece.*;

import java.util.LinkedList;
import java.util.List;

public class Solver {


    private Board plateau;
    private int h;

    public Solver(Board plateau) {
        this.plateau = plateau;
    }


    public Coup resolve(long timingOut){


        int i = 1;
        long timeStart = System.currentTimeMillis();
        Coup c = null;
        while(timeStart < timingOut) {
            Coup t = alphaBeta(null, i*2, -9999, 9999, plateau.isWhite(), true, timingOut);
            timeStart = System.currentTimeMillis();
            if(timeStart < timingOut){
                c = t;
            }
            i++;
        }
        //System.out.println("Profondeur :" + (i * 2) + "Coup trouvé :" + c);
        return c;
    }

    public Coup alphaBeta(Coup coup, int profondeur, int alpha, int beta, boolean couleur, boolean isMaximising, long timingOut) {

        if(profondeur == 0){
            return null;
        }

        Case[][] allCase = plateau.getAllCase();
        Coup bestCoup = null;

        if(isMaximising) {
            int bestScore = -9999;
            int i = 0;
            while(i < 8 && alpha < beta && System.currentTimeMillis() < timingOut){
                int j = 0;
                while(j < 8 && alpha < beta){
                    if (allCase[i][j].isPiece() && allCase[i][j].getPiece().getCote() == couleur) {
                        Coordonnees coordonnees = new Coordonnees(j, i);
                        int k = 0;
                        List<Coup> mouvPossible = allCase[i][j].getPiece().mouvementPossible(plateau, coordonnees);
                        List<Coup> mouvSansEchecPossible = Piece.retireCoupProvoquantEchec(plateau, mouvPossible, couleur);
                        while (k < mouvSansEchecPossible.size() && alpha < beta) {
                            Coup unCoup = removeCouleurUnCoup(this.plateau, mouvSansEchecPossible.get(k));
                            if (unCoup != null) {
                                plateau.moveCoup(unCoup);
                                Coup tempCoup = alphaBeta(unCoup, profondeur - 1, alpha, beta, !couleur, !isMaximising, timingOut);

                                plateau.undoLastCoup();
                                if (unCoup.getScore() != 0) {
                                    unCoup.addScore(profondeur);
                                }

                                unCoup.addScore((tempCoup != null ? tempCoup.getScore() : 0));
                                if (unCoup.getScore() > bestScore) {
                                    bestScore = unCoup.getScore();
                                    bestCoup = unCoup;
                                    bestCoup.setCoupSuivant(tempCoup);

                                }
                                /*if(alpha < bestScore){
                                    alpha = bestScore;
                                }*/
                            }
                            k++;
                        }
                    }
                    j++;
                }
                i++;
            }
        }else{
            int bestScore = 9999;
            int i = 0;
            while(i < 8 && alpha < beta && System.currentTimeMillis() < timingOut){
                int j = 0;
                while(j < 8 && alpha < beta){
                    if (allCase[i][j].isPiece() && allCase[i][j].getPiece().getCote() == couleur) {
                        Coordonnees coordonnees = new Coordonnees(j, i);

                        int k = 0;
                        List<Coup> mouvPossible = allCase[i][j].getPiece().mouvementPossible(plateau, coordonnees);
                        List<Coup> mouvSansEchecPossible = Piece.retireCoupProvoquantEchec(plateau, mouvPossible, couleur);
                        while (k < mouvSansEchecPossible.size() && alpha < beta) {
                            Coup unCoup = removeCouleurUnCoup(this.plateau, mouvSansEchecPossible.get(k));
                            if (unCoup != null) {
                                plateau.moveCoup(unCoup);

                                Coup tempCoup = alphaBeta(unCoup,profondeur-1,alpha,beta,!couleur,!isMaximising,timingOut);

                                if (unCoup.getScore() > 0) {
                                    unCoup.addScore(profondeur);
                                }

                                plateau.undoLastCoup();

                                unCoup.reverseScore();
                                unCoup.addScore((tempCoup != null ? tempCoup.getScore() : 0));

                                if (unCoup.getScore() < bestScore) {
                                    bestScore = unCoup.getScore();
                                    bestCoup = unCoup;
                                    bestCoup.setCoupSuivant(tempCoup);
                                }
                                /*if(beta > bestScore){
                                    beta = bestScore;
                                }*/
                            }
                            k++;
                        }
                    }
                    j++;
                }
                i++;
            }
        }
        return bestCoup;

    }

    /**
     * Renvoie la liste des mouvements possibles pour la couleur donné
     *
     * @param cote
     * @return
     */
    public static List<Coup> allMouvementsPossible(Board plateau, boolean cote) {
        List<Coup> coups = new LinkedList<>();

        Case[][] allCase = plateau.getAllCase();

        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                if (allCase[i][j].isPiece()) {
                    if (allCase[i][j].getPiece().getCote() == cote) {
                        Coordonnees coordonnees = new Coordonnees(j, i);
                        coups.addAll(allCase[i][j].getPiece().mouvementPossible(plateau, coordonnees));
                    }
                }
            }
        }

        return removeCouleur(plateau,coups);
    }

    public static List<Coup> removeCouleur(Board plateau,List<Coup> mouvementsPossible ) {

        List<Coup> resultat = new LinkedList<>();
        Case[][] allCase = plateau.getAllCase();

        for (Coup move : mouvementsPossible) {
            Coup rem = removeCouleurUnCoup(plateau,move);
            if(rem != null) {
                resultat.add(removeCouleurUnCoup(plateau,move));
            }
        }

        return resultat;
    }

    public static Coup removeCouleurUnCoup(Board plateau,Coup move) {

        Case[][] allCase = plateau.getAllCase();
        Coup resultat = null;

        Piece pieceDestination = allCase[move.getCoordonneesCible().getLigne()][move.getCoordonneesCible().getColonne()].getPiece();
        Piece pieceDepart = allCase[move.getCoordonneesDepart().getLigne()][move.getCoordonneesDepart().getColonne()].getPiece();

        if (pieceDestination != null) {
            if (pieceDestination.getCote() != pieceDepart.getCote())
            {
                resultat = move;
            }
        } else {
            resultat = move;
        }


        return resultat;
    }

    public int h()
    {
        boolean White = plateau.isWhite();
        int score=0;

        for(int i=0;i<8; i++)
        {
            for(int j=0;j<8;j++)
            {
                if (plateau.getAllCase()[i][j].getPiece()!=null) {
                    if (plateau.getAllCase()[i][j].getPiece().getCote() && White)
                        score += plateau.getAllCase()[i][j].getPiece().getScore();
                    else{
                        score -= plateau.getAllCase()[i][j].getPiece().getScore();
                    }
                }
            }
        }

        return White?score:-score;
    }

}

//TODO à débuguer : position startpos moves a2a3 e7e6 a3a4 b8c6 b2b3 a8b8 c2c3 d8f6 d2d3 b8a8 e2e3 f8e7 e3e4 e8f8 f2f3 e7c5 d3d4 c5d6 e4e5 f6g6 e5d6