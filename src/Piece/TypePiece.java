package Piece;

public enum TypePiece {
    Pion,
    Tour,
    Cavalier,
    Fou,
    Reine,
    Roi;
}
