package Piece;

import Jeu.Board;
import Jeu.Case;
import Jeu.Coordonnees;
import Jeu.Coup;

import java.util.ArrayList;
import java.util.List;

public class Roi extends Piece {
    private boolean hasMoved;

    public Roi(boolean cote) {
        nom = TypePiece.Roi;
        this.cote = cote;
        this.score = 900;

    }

    @Override
    public TypePiece TypePiece() {
        return this.nom;
    }

    @Override
    public List<Coup> mouvementPossible(Board board, Coordonnees coordonnees) {
        int colonne = coordonnees.getColonne();
        int ligne = coordonnees.getLigne();
        Case[][] allCase = board.getAllCase();

        ArrayList<Coup> mouvementPossiblesRoi = new ArrayList<>();
        if (ligne > 0) {
            mouvementPossiblesRoi.add(new Coup(coordonnees, new Coordonnees(colonne, ligne - 1)));
        }
        if (ligne < 7) {
            mouvementPossiblesRoi.add(new Coup(coordonnees, new Coordonnees(colonne, ligne + 1)));
        }
        if (colonne > 0) {
            mouvementPossiblesRoi.add(new Coup(coordonnees, new Coordonnees(colonne - 1, ligne)));
        }
        if (colonne < 7) {
            mouvementPossiblesRoi.add(new Coup(coordonnees, new Coordonnees(colonne + 1, ligne)));
        }
        if (colonne > 0 && ligne > 0) {
            mouvementPossiblesRoi.add(new Coup(coordonnees, new Coordonnees(colonne - 1, ligne - 1)));
        }
        if (colonne < 7 && ligne < 7) {
            mouvementPossiblesRoi.add(new Coup(coordonnees, new Coordonnees(colonne + 1, ligne + 1)));
        }
        if (colonne < 7 && ligne > 0) {
            mouvementPossiblesRoi.add(new Coup(coordonnees, new Coordonnees(colonne + 1, ligne - 1)));
        }
        if (colonne > 0 && ligne < 7) {
            mouvementPossiblesRoi.add(new Coup(coordonnees, new Coordonnees(colonne - 1, ligne + 1)));
        }
        if (getCote() && !board.hasWKingMoved())//Si on est du coté blanc et que le roi blanc n'a pas bougé
        {
            if ((!board.hasWTourMovedLeft()))//Si la tour gauche n'a pas bougé
            {
                if (board.getAllCase()[ligne][colonne - 1].getPiece() == null && board.getAllCase()[ligne][colonne - 2].getPiece() == null && board.getAllCase()[ligne][colonne - 3].getPiece() == null)//Si les deux cases à gauches sont vides
                {

                    if ((!board.echec(new Coordonnees(colonne - 1, ligne), getCote())) && (!board.echec(new Coordonnees(colonne - 2, ligne), getCote())) && (!board.echec(new Coordonnees(colonne - 3, ligne), getCote())))//Et qu'elles ne sont pas contrôlé par l'adversaire
                    {

                        Coup coup = new Coup(coordonnees, new Coordonnees(colonne - 3, ligne));
                        coup.addScore(120);

                        mouvementPossiblesRoi.add(coup);
                    }


                }

            }
            if ((!board.hasWTourMovedRight()))//Si la tour droite n'a pas bougé
            {

                if (board.getAllCase()[ligne][colonne + 1].getPiece() == null && board.getAllCase()[ligne][colonne + 2].getPiece() == null)//Si les deux cases à gauches sont vides
                {
                    if ((!board.echec(new Coordonnees(colonne + 1, ligne), getCote())) && (!board.echec(new Coordonnees(colonne + 2, ligne), getCote())))//Et qu'elles ne sont pas contrôlé par l'adversaire
                    {

                        Coup coup = new Coup(coordonnees, new Coordonnees(colonne + 2, ligne));
                        coup.addScore(120);
                        mouvementPossiblesRoi.add(coup);
                    }


                }

            }


        } else if ((!getCote()) && !board.hasBKingMoved())//Si on est du coté noir et que le roi noir n'a pas bougé
        {

            if ((!board.hasBTourMovedLeft()))//Si la tour gauche n'a pas bougé
            {
                if (board.getAllCase()[ligne][colonne - 1].getPiece() == null && board.getAllCase()[ligne][colonne - 2].getPiece() == null && board.getAllCase()[ligne][colonne - 3].getPiece() == null)//Si les deux cases à gauches sont vides
                {

                    if ((!board.echec(new Coordonnees(colonne - 1, ligne), getCote())) && (!board.echec(new Coordonnees(colonne - 2, ligne), getCote())) && (!board.echec(new Coordonnees(colonne - 3, ligne), getCote())))//Et qu'elles ne sont pas contrôlé par l'adversaire
                    {
                        Coup coup = new Coup(coordonnees, new Coordonnees(colonne - 3, ligne));
                        coup.addScore(120);
                        mouvementPossiblesRoi.add(coup);
                    }

                }

            }
            if ((!board.hasBTourMovedRight()))//Si la tour gauche n'a pas bougé
            {
                if (board.getAllCase()[ligne][colonne + 1].getPiece() == null && board.getAllCase()[ligne][colonne + 2].getPiece() == null)//Si les deux cases à gauches sont vides
                {
                    if ((!board.echec(new Coordonnees(colonne + 1, ligne), getCote())) && (!board.echec(new Coordonnees(colonne + 2, ligne), getCote())))//Et qu'elles ne sont pas contrôlé par l'adversaire
                    {
                        Coup coup = new Coup(coordonnees, new Coordonnees(colonne + 2, ligne));
                        coup.addScore(120);
                        mouvementPossiblesRoi.add(coup);
                    }

                }

            }


        }


        return mouvementPossiblesRoi;
    }

    @Override
    public int getScore() {
        return this.score;
    }

    public static List<Coup> mouvementsPossibleRoi(Board board, Coordonnees coordonnees) {
        Roi roi = new Roi(true); // On s'en fou
        return roi.mouvementPossible(board, coordonnees);
    }

    @Override
    public String toString() {
        String couleur;
        if (cote) {
            couleur = " B";
        } else {
            couleur = " N";
        }
        return "Roi     " + couleur;
    }
}
