package Piece;

import Jeu.Board;
import Jeu.Case;
import Jeu.Coordonnees;
import Jeu.Coup;

import java.util.ArrayList;
import java.util.List;

public class Fou extends Piece {

    public Fou(boolean cote) {
        nom = TypePiece.Fou;
        this.cote = cote;
        this.score = 33;
    }

    @Override
    public TypePiece TypePiece() {
        return nom;
    }

    @Override
    public List<Coup> mouvementPossible(Board board, Coordonnees coordonnees) {
        int colonne = coordonnees.getColonne();
        int ligne = coordonnees.getLigne();
        Case[][] allCase = board.getAllCase();

        ArrayList<Coup> mouvementPossiblesFou = new ArrayList<>();

        for (int i = 1; (ligne - i >= 0) && (colonne - i >= 0); i++)//Haut gauche
        {
            if (allCase[ligne - i][colonne - i].isPiece()) {
                mouvementPossiblesFou.add(new Coup(coordonnees,new Coordonnees(colonne - i, ligne - i)));
                break;
            } else {
                mouvementPossiblesFou.add(new Coup(coordonnees,new Coordonnees(colonne - i, ligne - i)));
            }

        }
        for (int i = 1; (ligne + i < 8) && (colonne + i < 8); i++)//Bas droite
        {
            if (allCase[ligne + i][colonne + i].isPiece()) {
                mouvementPossiblesFou.add(new Coup(coordonnees,new Coordonnees(colonne + i, ligne + i)));
                break;
            } else {
                mouvementPossiblesFou.add(new Coup(coordonnees,new Coordonnees(colonne + i, ligne + i)));
            }

        }

        for (int i = 1; (ligne - i >= 0) && (colonne + i < 8); i++)//Haut droite
        {

            if (allCase[ligne - i][colonne + i].isPiece()) {
                mouvementPossiblesFou.add(new Coup(coordonnees,new Coordonnees(colonne + i, ligne - i)));
                break;
            } else {
                mouvementPossiblesFou.add(new Coup(coordonnees,new Coordonnees(colonne + i, ligne - i)));
            }

        }

        for (int i = 1; (ligne + i < 8) && (colonne - i >= 0); i++)//Bas gauche
        {
            if (allCase[ligne + i][colonne - i].isPiece()) {
                mouvementPossiblesFou.add(new Coup(coordonnees,new Coordonnees(colonne - i, ligne + i)));
                break;
            } else {
                mouvementPossiblesFou.add(new Coup(coordonnees,new Coordonnees(colonne - i, ligne + i)));
            }

        }


        return mouvementPossiblesFou;
    }

    @Override
    public int getScore() {
        return this.score;
    }

    /**
     * Utile pour la dame
     * @param board
     * @param coordonnees
     * @return
     */
    public static List<Coup>  mouvementsPossibleFou(Board board, Coordonnees coordonnees){
        Fou fou = new Fou(true); // On s'en fou
        return fou.mouvementPossible(board,coordonnees);
    }

    @Override
    public String toString() {
        String couleur;
        if(cote){
            couleur=" B";
        }
        else{
            couleur=" N";
        }return "Fou     "+couleur;
    }
}
