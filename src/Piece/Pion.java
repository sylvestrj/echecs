package Piece;

import Jeu.Board;
import Jeu.Case;
import Jeu.Coordonnees;
import Jeu.Coup;

import java.util.ArrayList;
import java.util.List;

public class Pion extends Piece {


    public Pion(boolean cote) {
        nom = TypePiece.Pion;
        this.cote = cote;
        this.score = 20;
    }

    @Override
    public TypePiece TypePiece() {
        return nom;
    }

    @Override
    public List<Coup> mouvementPossible(Board board, Coordonnees coordonnees) {

        int colonne = coordonnees.getColonne();
        int ligne = coordonnees.getLigne();
        Case allCase[][] = board.getAllCase();

        int white = 1;
        if (!cote) // Pour différencier sens dans lequels les pions se dirigent
        {
            white = -1;
        }

        ArrayList<Coup> mouvementPossiblesPion = new ArrayList<>();

        if (colonne > 0) { //Vérirfication à gauche
            if (ligne > 0 && white > 0) {

                if (allCase[ligne - 1][colonne - 1].isPiece()) {

                    Coup coup = new Coup(coordonnees, new Coordonnees(colonne - 1, ligne - white));
                    setPromotionScore(ligne - white, coup);
                    mouvementPossiblesPion.add(coup);
                }
            }
            if (ligne < 7 && white < 0) {
                if (allCase[ligne + 1][colonne - 1].isPiece()) {
                    Coup coup = new Coup(coordonnees, new Coordonnees(colonne - 1, ligne - white));
                    setPromotionScore(ligne - white, coup);
                    mouvementPossiblesPion.add(coup);
                }
            }
        }

        if (ligne > 0 && ligne < 7) {
            if (!allCase[ligne - white][colonne].isPiece())//Vérification devant
            {
                Coup coup = new Coup(coordonnees, new Coordonnees(colonne, ligne - white));
                setPromotionScore(ligne - white, coup);
                mouvementPossiblesPion.add(coup);
            }
        }

        if (colonne < 7)//vérification à droite
        {
            if (ligne > 0 && white > 0) {
                if (allCase[ligne - 1][colonne + 1].isPiece()) {
                    Coup coup = new Coup(coordonnees, new Coordonnees(colonne + 1, ligne - white));
                    setPromotionScore(ligne - white, coup);
                    mouvementPossiblesPion.add(coup);
                }
            }
            if (ligne < 7 && white < 0)//J'ai mis cettte disposition pour tenter de minimiser le nombre de if
            {
                if (allCase[ligne + 1][colonne + 1].isPiece()) {
                    Coup coup = new Coup(coordonnees, new Coordonnees(colonne + 1, ligne - white));
                    setPromotionScore(ligne - white, coup);
                    mouvementPossiblesPion.add(coup);
                }
            }

        }

        //TODO avance de 2 si au point de départ
        //TODO Prise en passant
        return mouvementPossiblesPion;
    }

    @Override
    public int getScore() {
        return this.score;
    }

    public static List<Coup> mouvementsPossiblePion(Board board, Coordonnees coordonnees) {
        Pion pion = new Pion(true); // On s'en fou
        return pion.mouvementPossible(board, coordonnees);
    }

    @Override
    public String toString() {
        String couleur;
        if (cote) {
            couleur = " B";
        } else {
            couleur = " N";
        }
        return "Pion    " + couleur;
    }

    /**
     * Mise à jour du score si promotion
     *
     * @param ligne La ligne atteint après le déplacement
     * @param coup  Le coup en cours
     */

    private void setPromotionScore(int ligne, Coup coup) {
        if (ligne == 8 || ligne == 0) { //Si on va atteindre le bout du plateau
            coup.addScore(900);
        }
    }
}
