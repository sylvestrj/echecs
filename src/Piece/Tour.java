package Piece;

import Jeu.Board;
import Jeu.Case;
import Jeu.Coordonnees;
import Jeu.Coup;

import java.util.ArrayList;
import java.util.List;

public class Tour extends Piece {


    public Tour(boolean cote) {
        nom = TypePiece.Tour;
        this.cote = cote;
        this.score = 50;

    }

    @Override
    public TypePiece TypePiece() {
        return nom;
    }

    @Override
    public List<Coup> mouvementPossible(Board board, Coordonnees coordonnees) {
        int colonne = coordonnees.getColonne();
        int ligne = coordonnees.getLigne();
        Case[][] allCase = board.getAllCase();

        ArrayList<Coup> mouvementPossiblesTour = new ArrayList<>();
        for (int i = ligne - 1; i >= 0; i--) {

            if (allCase[i][colonne].isPiece()) {

                mouvementPossiblesTour.add(new Coup(coordonnees, new Coordonnees(colonne, i)));
                break;
            } else {
                mouvementPossiblesTour.add(new Coup(coordonnees, new Coordonnees(colonne, i)));
            }
        }
        for (int i = ligne + 1; i < 8; i++) {
            if (allCase[i][colonne].isPiece()) {
                mouvementPossiblesTour.add(new Coup(coordonnees, new Coordonnees(colonne, i)));
                break;
            } else {
                mouvementPossiblesTour.add(new Coup(coordonnees, new Coordonnees(colonne, i)));
            }

        }
        for (int i = colonne - 1; i >= 0; i--) {
            if (allCase[ligne][i].isPiece()) {
                mouvementPossiblesTour.add(new Coup(coordonnees, new Coordonnees(i, ligne)));
                break;
            } else {
                mouvementPossiblesTour.add(new Coup(coordonnees, new Coordonnees(i, ligne)));
            }

        }
        for (int i = colonne + 1; i < 8; i++) {
            if (allCase[ligne][i].isPiece()) {
                mouvementPossiblesTour.add(new Coup(coordonnees, new Coordonnees(i, ligne)));
                break;
            } else {
                mouvementPossiblesTour.add(new Coup(coordonnees, new Coordonnees(i, ligne)));
            }

        }
        return mouvementPossiblesTour;
    }

    @Override
    public int getScore() {
        return this.score;
    }

    /**
     * Utile pour la dame
     *
     * @param board
     * @param coordonnees
     * @return
     */
    public static List<Coup> mouvementsPossibleTour(Board board, Coordonnees coordonnees) {
        Tour tour = new Tour(true); // On s'en fou
        return tour.mouvementPossible(board, coordonnees);
    }


    @Override
    public String toString() {
        String couleur;
        if (cote) {
            couleur = " B";
        } else {
            couleur = " N";
        }
        return "Tour    " + couleur;
    }
}
