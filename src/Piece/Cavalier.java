package Piece;

import Jeu.Board;
import Jeu.Case;
import Jeu.Coordonnees;
import Jeu.Coup;

import java.util.ArrayList;
import java.util.List;

public class Cavalier extends Piece {


    public Cavalier(boolean cote) {
        nom = TypePiece.Cavalier;
        this.cote = cote;
        this.score=32;
    }

    @Override
    public TypePiece TypePiece() {
        return nom;
    }

    @Override
    public List<Coup> mouvementPossible(Board board, Coordonnees coordonnees) {

        int colonne = coordonnees.getColonne();
        int ligne = coordonnees.getLigne();

        ArrayList<Coup> mouvementPossiblesCavalier = new ArrayList<>();
        for (int i = ligne - 2; i <= ligne + 2; i++) {
            for (int j = colonne - 2; j <= colonne + 2; j++) {
                if (Math.abs(ligne - i) + Math.abs(colonne - j) == 3)//On récupère touce qui a une ditance de 3 dans un carré 4x4
                {
                    if ((i >= 0) && (j >= 0) && (i < 8) && (j < 8)) {
                        mouvementPossiblesCavalier.add(new Coup(coordonnees,new Coordonnees(j, i)));
                    }
                }
            }
        }
        return mouvementPossiblesCavalier;
    }

    @Override
    public int getScore() {
        return this.score;
    }

    public static List<Coup>  mouvementsPossibleCavalier(Board board, Coordonnees coordonnees){
        Cavalier cavalier = new Cavalier(true); // On s'en fou
        return cavalier.mouvementPossible(board,coordonnees);
    }

    @Override
    public String toString() {
        String couleur;
        if (cote) {
            couleur = " B";
        } else {
            couleur = " N";
        }
        return "Cavalier" + couleur;
    }
}
