package Piece;


import Jeu.Board;
import Jeu.Coordonnees;
import Jeu.Coup;

import java.util.ArrayList;
import java.util.List;

public abstract class Piece {
    public TypePiece nom;
    public boolean cote;// White = true   Black = false
    public int score;

    public abstract TypePiece TypePiece();

    public boolean getCote() {
        return cote;
    }

    public void setCote(boolean cote) {
        this.cote = cote;
    }

    abstract public List<Coup> mouvementPossible(Board board, Coordonnees coordonnees);

    abstract public int getScore();

    /**
     * @param board
     * @param coupsPossibles
     * @param coteJoueur     Le coté du joueur ayant les coups possibles
     */
    static public List<Coup> retireCoupProvoquantEchec(Board board, List<Coup> coupsPossibles, boolean coteJoueur) {

        List<Coup> coupsSansEchec = new ArrayList<>();


        for (Coup coup : coupsPossibles) {

            board.moveCoup(coup);
            //On récupére les coordonnées du Roi
            Coordonnees coordonneesRoi;
            if (coteJoueur) coordonneesRoi = board.getCoordonneesRoiBlanc();
            else coordonneesRoi = board.getCoordonneesRoiNoir();

//            System.out.println(coup);
//            System.out.println("Move Secure:"+!board.echec(coordonneesRoi,coteJoueur));

            //Si on est pas en échec on prend le coup
            if (!board.echec(coordonneesRoi, coteJoueur)) coupsSansEchec.add(coup);

            board.undoLastCoup();
        }

        return coupsSansEchec;
    }

}
