package Piece;

import Jeu.Board;
import Jeu.Coordonnees;
import Jeu.Coup;

import java.util.ArrayList;
import java.util.List;

public class Reine extends Piece {


    public Reine(boolean cote) {
        nom = TypePiece.Reine;
        this.cote = cote;
        this.score = 90;
    }

    @Override
    public TypePiece TypePiece() {
        return nom;
    }

    @Override
    public List<Coup> mouvementPossible(Board board, Coordonnees coordonnees) {

        ArrayList<Coup> mouvementPossiblesReine = new ArrayList<>();
        mouvementPossiblesReine.addAll(Fou.mouvementsPossibleFou(board, coordonnees));
        mouvementPossiblesReine.addAll(Tour.mouvementsPossibleTour(board, coordonnees));

        return mouvementPossiblesReine;
    }

    @Override
    public int getScore() {
        return this.score;
    }

    public static List<Coup> mouvementsPossibleReine(Board board, Coordonnees coordonnees) {
        Reine reine = new Reine(true); // On s'en fou
        return reine.mouvementPossible(board, coordonnees);
    }

    @Override
    public String toString() {
        String couleur;
        if (cote) {
            couleur = " B";
        } else {
            couleur = " N";
        }
        return "Reine   " + couleur;
    }
}
