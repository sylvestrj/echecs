import Jeu.Board;
import Jeu.Coordonnees;
import Jeu.Coup;
import Jeu.Solver;
import Piece.Roi;

import java.util.*;

public class UCI {
    static String ENGINENAME = "Lasagna";


    public static boolean startpos;
    public static Board plateau;
    public static Solver solver;

    //On le mt à -1 si on ne l'utilise plus
    public static int nbTours = 0;
    public static ArrayList<Coup> ouvertureBlanc = new ArrayList() {{
        add(new Coup("e2e4"));
        add(new Coup("b1c3"));
        add(new Coup("g1f3"));
        add(new Coup("d2d4"));
    }};

    public static ArrayList<Coup> ouvertureNoir = new ArrayList() {{
        add(new Coup("g7g6"));
        add(new Coup("g8f6"));
        add(new Coup("f8h6"));
        add(new Coup("e8g8"));
    }};


    public static void uciCommunication() {
        setStartpos(true);
        Scanner input = new Scanner(System.in);

        while (true) {
            String inputString = input.nextLine();
            if ("uci".equals(inputString)) {
                inputUCI();
            } else if (inputString.startsWith("setoption")) {
                inputSetOption(inputString);
            } else if ("isready".equals(inputString)) {
                inputIsReady();
            } else if ("ucinewgame".equals(inputString)) {
                inputUCINewGame();
            } else if (inputString.startsWith("position")) {
                inputPosition(inputString);
            } else if (inputString.startsWith("go")) {
                inputGo(inputString);
            } else if (inputString.equals("quit")) {
                inputQuit();
            } else if ("print".equals(inputString)) {
                inputPrint();
            }
        }
    }

    public static void setStartpos(boolean startpos) {
        UCI.startpos = startpos;
    }

    public static void inputUCI() {
        System.out.println("id name " + ENGINENAME);
        System.out.println("id author Paul-Alexis, Alexy, Johen");
        //options go here
        System.out.println("uciok");
    }

    public static void inputSetOption(String inputString) {
        //set options
    }

    public static void inputIsReady() {
        System.out.println("readyok");
    }

    public static void inputUCINewGame() {
        nbTours = 0;
    }

    public static void inputPosition(String input) {
        input = input.substring(9).concat(" ");
        if (input.contains("startpos ") && startpos) {

            input = input.substring(9);
            plateau = new Board();
            solver = new Solver(plateau);

        }

        if (input.contains("moves")) {

            input = input.substring(input.indexOf("moves") + 6);
            while (input.length() > 0) {
                plateau.moveString(input);

                input = input.substring(input.indexOf(' ') + 1);
            }
        }
    }

    public static void inputGo(String inputString) {
        int timeInMillis = 1000;
        if (inputString.contains("movetime")) {
            timeInMillis = Integer.parseInt(inputString.split("movetime ")[1]);
        }

        String move = "";
        Coordonnees coordonneesRoi = plateau.isWhite() ? plateau.getCoordonneesRoiBlanc() : plateau.getCoordonneesRoiNoir();
        Coordonnees coordonneesRoiCote = new Coordonnees(coordonneesRoi.getColonne() + 1, coordonneesRoi.getLigne());

        //Si on est en echec on annule le pattern
        if (plateau.echec(coordonneesRoi, plateau.isWhite()) || plateau.echec(coordonneesRoiCote, plateau.isWhite())) {
            nbTours = -1;
        }
        if (nbTours >= 0 && nbTours < 4) {


            //Si le nombre de tour est entre 0 et 3 on force la pattern

            ArrayList<Coup> ouvertures;

            if (plateau.isWhite()) {
                ouvertures = ouvertureBlanc;
            } else {
                ouvertures = ouvertureNoir;
            }

            Coup c = ouvertures.get(nbTours);
            move = Coup.parseCoup(c.getCoordonneesDepart());
            move += Coup.parseCoup(c.getCoordonneesCible());
            System.out.println("bestmove " + move);

            System.out.println(c);

            nbTours++;
        } else {


            long timingOut = System.currentTimeMillis() + timeInMillis - 75;
            //System.out.println(timingOut);

            Coup c = solver.resolve(timingOut);
            move = Coup.parseCoup(c.getCoordonneesDepart());
            move += Coup.parseCoup(c.getCoordonneesCible());
            System.out.println("bestmove " + move);
            //System.out.println("Score : " + c.getScore());
            //System.out.println(c);
        }


    }

    public static void inputQuit() {
        System.exit(0);
    }

    public static void inputPrint() {
        System.out.println(plateau);
    }
}